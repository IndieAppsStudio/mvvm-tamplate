<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
    <#include "root://activities/common/recipe_manifest.xml.ftl" />
    <@kt.addAllKotlinDependencies />

    <instantiate from="root/res/layout/activity.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(activityLayout)}.xml" />
    <instantiate from="root/res/layout/fragment.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentLayout)}.xml" />
    <instantiate from="root/res/layout/item.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(itemLayout)}.xml" />
    <open file="${escapeXmlAttribute(resOut)}/layout/${fragmentLayout}.xml" />

    <instantiate from="root/src/app_package/Activity.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${activityClass}.kt" />

    <instantiate from="root/src/app_package/Fragment.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${fragmentClass}.kt" />

    <instantiate from="root/src/app_package/ViewModel.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${viewModelClass}.kt" />

    <instantiate from="root/src/app_package/UserActionsListener.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${userActionsListenerClass}.kt" />

    <instantiate from="root/src/app_package/Bindings.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${bindingsClass}.kt" />

    <instantiate from="root/src/app_package/Adapter.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${adapterClass}.kt" />

</recipe>
