<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View.OnClickListener" />

        <variable
                name="listener"
                type="${escapeKotlinIdentifiers(packageName)}.${userActionsListenerClass}" />

        <variable
                name="model"
                type="String" />
    </data>

    <androidx.cardview.widget.CardView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="2dp"
            android:layout_marginTop="2dp"
            app:cardCornerRadius="4dp"
            android:onClick="@{() -> listener.onItemClicked(model)}"
            app:cardUseCompatPadding="true">

        <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:orientation="vertical"
                android:gravity="center_vertical">

                <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        tools:text="Item"
                        android:text="@{model}"/>

        </LinearLayout>

    </androidx.cardview.widget.CardView>
</layout>