<?xml version="1.0" encoding="utf-8"?>

<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        tools:context="${packageName}.${activityClass}">

    <data>
        <import type="android.view.View.OnClickListener" />
        <import type="android.view.View" />
        <variable
                name="listener"
                type="${escapeKotlinIdentifiers(packageName)}.${userActionsListenerClass}" />

        <variable
                name="viewmodel"
                type="${escapeKotlinIdentifiers(packageName)}.${viewModelClass}" />
    </data>

    <ScrollView
            android:id="@+id/${classToResource(fragmentClass)}"
            android:layout_width="match_parent"
            android:layout_height="match_parent">
    </ScrollView>

        <RelativeLayout
            android:id="@+id/${classToResource(fragmentClass)}"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:gravity="center">

        <androidx.swiperefreshlayout.widget.SwipeRefreshLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                app:onRefreshListener="@{() -> viewmodel.onRefresh()}"
                app:refreshing="@{viewmodel.isLoading}">

            <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/recycler_models"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    app:modelList="@{viewmodel.models}"
                    android:paddingLeft="8dp"
                    android:paddingRight="8dp"
                    android:clipToPadding="false"
                    android:paddingBottom="90dp"/>

        </androidx.swiperefreshlayout.widget.SwipeRefreshLayout>

        <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical"
                android:gravity="center"
                android:visibility="@{viewmodel.isEmpty()? View.VISIBLE : View.GONE}"
                android:padding="16dp">

            <ImageView
                    android:layout_width="70dp"
                    android:layout_height="70dp"
                    android:src="@drawable/ic_beach_access_black_24dp"
                    android:tint="@android:color/darker_gray"
                    android:contentDescription="@string/description_icon_empty" />

            <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/title_list_empty"
                    android:textStyle="bold"
                    android:textSize="14sp"
                    android:textColor="@android:color/darker_gray"
                    android:textAlignment="center"/>
        </LinearLayout>

        <com.google.android.material.floatingactionbutton.FloatingActionButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/fab_margin"
                app:srcCompat="@drawable/ic_add_black_24dp"
                tools:ignore="VectorDrawableCompat"
                android:tint="@android:color/white"
                android:layout_alignParentEnd="true"
                android:layout_alignParentBottom="true"
                android:onClick="@{() -> listener.onAddClicked()}"/>
    </RelativeLayout>
</layout>
