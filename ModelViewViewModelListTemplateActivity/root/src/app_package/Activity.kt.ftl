package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
<#if applicationPackage??>
  import ${applicationPackage?replace('.debug', '')}.R
  import ${applicationPackage?replace('.debug', '')}.base.BaseActivity
  import ${applicationPackage?replace('.debug', '')}.util.obtainViewModel
  import ${applicationPackage?replace('.debug', '')}.util.replaceFragmentInActivity
</#if>

class ${activityClass} : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.${activityLayout})

        setupFragment()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.${classToResource(activityClass)}_container)
        replaceFragmentInActivity(${fragmentClass}.newInstance(), R.id.${classToResource(activityClass)}_container)
    }

    fun obtainViewModel(): ${viewModelClass} = obtainViewModel(${viewModelClass}::class.java)
}
