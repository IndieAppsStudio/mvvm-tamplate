package ${escapeKotlinIdentifiers(packageName)}

class ${userActionsListenerClass}(
    private var dataset: List<String>,
    private val listener: ${userActionsListenerClass}
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: ${underscoreToCamelCase(itemLayout)}Binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.${itemLayout}, parent, false)

        return ModelItemHolder(binding)

    }

    override fun getItemCount(): Int = dataset.size

    fun updateDataset(dataset: List<String>){
        this.dataset = dataset
        this.notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val paymentType = dataset[position]
        (holder as ModelItemHolder).bindItem(paymentType, listener)
    }

    class ModelItemHolder(itemView: ${underscoreToCamelCase(itemLayout)}Binding) : RecyclerView.ViewHolder(itemView.root) {
        val binding: ${underscoreToCamelCase(itemLayout)}Binding = itemView

        fun bindItem(model: String, userActionListener: ${userActionsListenerClass}) {
            binding.model = model
            binding.listener = userActionListener
            binding.executePendingBindings()
        }
    }
}
