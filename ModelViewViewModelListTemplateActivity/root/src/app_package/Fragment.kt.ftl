package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
<#if applicationPackage??>
  import ${applicationPackage?replace('.debug', '')}.base.BaseFragment
  import ${applicationPackage?replace('.debug', '')}.databinding.${underscoreToCamelCase(fragmentLayout)}Binding
  import ${applicationPackage?replace('.debug', '')}.util.setupStringSnackbar
</#if>

class ${fragmentClass} : BaseFragment(), ${userActionsListenerClass}{


    private lateinit var viewDataBinding: ${underscoreToCamelCase(fragmentLayout)}Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = ${underscoreToCamelCase(fragmentLayout)}Binding.inflate(inflater, container, false).apply {
            viewmodel = (activity as ${activityClass}).obtainViewModel()
            listener = this@${fragmentClass}
        }

        subscription()
        setupAdapter()

        return viewDataBinding.root
    }

    override fun onResume() {
        super.onResume()
        viewDataBinding.viewmodel?.start()
    }

    private fun subscription() {
        // if error change first character to lower case eg: RegisterUser -> registerUser
        viewDataBinding.viewmodel?.let {
            viewDataBinding.${underscoreToCamelCase(classToResource(fragmentClass))}.setupStringSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
    }

    private fun setupAdapter() {
        val mViewModel = viewDataBinding.viewmodel
        if (mViewModel != null) {
            val adapter = ${adapterClass}(emptyList(), this)
            viewDataBinding.recyclerModels.adapter = adapter
            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
            viewDataBinding.recyclerModels.layoutManager = layoutManager
        }
    }

    fun onItemClicked(model: String){
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun onAddClicked(){
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {

        fun newInstance(): ${fragmentClass} {
            val fragment = ${fragmentClass}()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
