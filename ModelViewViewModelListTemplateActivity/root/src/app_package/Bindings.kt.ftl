package ${escapeKotlinIdentifiers(packageName)}

object ${bindingsClass} {

    @BindingAdapter("modelList")
    @JvmStatic
    fun setModelList(recyclerView: RecyclerView, models: List<String>) {
        with(recyclerView.adapter as ${adapterClass}) {
            updateDataset(models)
        }
    }
}
