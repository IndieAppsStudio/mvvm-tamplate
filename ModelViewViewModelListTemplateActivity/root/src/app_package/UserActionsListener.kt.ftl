package ${escapeKotlinIdentifiers(packageName)}

interface ${userActionsListenerClass} {

    fun onItemClicked(model: String)

    fun onAddClicked()
}
