package ${escapeKotlinIdentifiers(packageName)}

import android.app.Application
import androidx.databinding.ObservableField
<#if applicationPackage??>
  import ${applicationPackage?replace('.debug', '')}.base.BaseViewModel
  import ${applicationPackage?replace('.debug', '')}.data.source.AppRepository
  import ${applicationPackage?replace('.debug', '')}.util.SingleLiveEvent
</#if>

class ${viewModelClass} (
    context: Application,
    val appRepository: AppRepository
): BaseViewModel(context, appRepository) {
    var isLoading = ObservableField<Boolean>()
    var isEmpty = ObservableField<Boolean>()
    val models: ObservableList<String> = ObservableArrayList()

    val snackbarMessage = SingleLiveEvent<String>()

    fun start() {
        isLoading.set(true)
        setDummy()
    }

    private fun setDummy() {
        val listDummy = arrayListOf("1", "2", "3", ".", ".")
        onSuccess(listDummy)
    }

    fun onRefresh() {
        start()
    }

    fun onSuccess(models: List<String>) {
        isLoading.set(false)
        isEmpty.set(false)
        with(this@${viewModelClass}.models){
            clear()
            addAll(models)
        }
    }

    fun onError(message: String) {
        isLoading.set(false)
        snackbarMessage.postValue(message)
    }

    fun onEmpty() {
        isLoading.set(false)
        isEmpty.set(true)
        this.models.clear()
    }
}
