<?xml version="1.0" encoding="utf-8"?>

<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        tools:context="${packageName}.${activityClass}">

    <data>
        <import type="android.view.View.OnClickListener" />
        <import type="android.view.View" />
        <variable
                name="listener"
                type="${escapeKotlinIdentifiers(packageName)}.${userActionsListenerClass}" />

        <variable
                name="viewmodel"
                type="${escapeKotlinIdentifiers(packageName)}.${viewModelClass}" />
    </data>

    <ScrollView
            android:id="@+id/${classToResource(fragmentClass)}"
            android:layout_width="match_parent"
            android:layout_height="match_parent">

        <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:padding="16dp">

            <androidx.cardview.widget.CardView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:padding="16dp"
                    app:cardCornerRadius="4dp">

                <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:padding="16dp"
                        android:text="@{viewmodel.hello}" />
            </androidx.cardview.widget.CardView>

        </LinearLayout>
    </ScrollView>
</layout>
