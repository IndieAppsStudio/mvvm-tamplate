package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
<#if applicationPackage??>
  import ${applicationPackage?replace('.debug', '')}.base.BaseFragment
  import ${applicationPackage?replace('.debug', '')}.databinding.${underscoreToCamelCase(fragmentLayout)}Binding
  import ${applicationPackage?replace('.debug', '')}.util.setupStringSnackbar
</#if>

class ${fragmentClass} : BaseFragment(), ${userActionsListenerClass}{


    private lateinit var viewDataBinding: ${underscoreToCamelCase(fragmentLayout)}Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = ${underscoreToCamelCase(fragmentLayout)}Binding.inflate(inflater, container, false).apply {
            viewmodel = (activity as ${activityClass}).obtainViewModel()
            listener = this@${fragmentClass}
        }

        subscription()

        return viewDataBinding.root
    }

    override fun onResume() {
        super.onResume()
        viewDataBinding.viewmodel?.start()
    }

    private fun subscription() {
        viewDataBinding.viewmodel?.let {
            viewDataBinding.${underscoreToCamelCase(classToResource(fragmentClass))}.setupStringSnackbar(this, it.snackbarMessage, Snackbar.LENGTH_LONG)
        }
    }

    companion object {

        fun newInstance(): ${fragmentClass} {
            val fragment = ${fragmentClass}()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
