package ${escapeKotlinIdentifiers(packageName)}

import android.app.Application
import androidx.databinding.ObservableField
<#if applicationPackage??>
  import ${applicationPackage?replace('.debug', '')}.base.BaseViewModel
  import ${applicationPackage?replace('.debug', '')}.data.source.AppRepository
  import ${applicationPackage?replace('.debug', '')}.util.SingleLiveEvent
</#if>

class ${viewModelClass} (
    context: Application,
    appRepository: AppRepository
): BaseViewModel(context, appRepository) {
    var hello = ObservableField<String>()
    val snackbarMessage = SingleLiveEvent<String>()

    fun start() {
        hello.set("Holla")
    }
}
